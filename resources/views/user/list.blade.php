
@extends('layout.master')

@section('content')
<div style="background-color: white">
  <table class="table table-bordered">
    <thead>                  
      <tr>
        <th style="width: 10px">No</th>
        <th>Username</th>
        <th>Password</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>1.</td>
        <td>Update software</td>
        <td>
          <div class="progress progress-xs">
            <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
          </div>
        </td>
        <td><span class="badge bg-danger">55%</span></td>
        <td><span class="badge bg-danger">55%</span></td>
        <td>
        <button class="btn btn-primary btn-sm">Update</button><br><br>
        <button class="btn btn-danger btn-sm">Delete</button>
      </td>
      </tr>
      <tr>
    </tbody>
  </table>
</div>
 
@endsection


