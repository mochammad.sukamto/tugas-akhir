@extends('layout.master')

@section('content')
<div style="background-color: white">
     <div class="card-header">
                    <h3 class="card-title">Create New User</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/animes" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="judul">Username</label>
                            <input type="text" class="form-control" id="judul" name="judul" placeholder="Masukkan Judul Anime">
                        </div>
                        <div class="form-group">
                            <label for="rincian">Password</label>
                            <input type="text" class="form-control" id="rincian" name="rincian" placeholder="Masukkan  Anime">
                        </div>
                        <div class="form-group">
                            <label for="tahun">Nama</label>
                            <input type="text" class="form-control" id="tahun" name="tahun" placeholder="Masukkan  Anime">
                        </div>
                        <div class="form-group">
                            <label for="gambar">Email</label>
                            <input type="text" class="form-control" id="gambar" name="gambar" placeholder="Masukkan  Anime">
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-danger">Create</button>
                        </div>
                    </div>
                </form>


</div>
@endsection