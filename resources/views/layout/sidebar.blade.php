   <div class="product__sidebar">
        <div class="product__sidebar__view">
            <div class="section-title">
                <h5>Top Views</h5>
            </div>
            <div class="filter__gallery">
                <div class="product__sidebar__view__item set-bg mix day years"
                data-setbg="img/sidebar/tv-1.jpg">
                    <div class="ep">18 / ?</div>
                    <div class="view"><i class="fa fa-eye"></i> 9141</div>
                    <h5><a href="#">Boruto: Naruto next generations</a></h5>
                </div>
            </div>
            <div class="product__sidebar__comment">
                <div class="section-title">
                    <h5>New Comment</h5>
                </div>
                <div class="product__sidebar__comment__item">
                    <div class="product__sidebar__comment__item__pic">
                        <img src="img/sidebar/comment-1.jpg" alt="">
                    </div>
                    <div class="product__sidebar__comment__item__text">
                        <ul>
                            <li>Active</li>
                            <li>Movie</li>
                        </ul>
                        <h5><a href="#">The Seven Deadly Sins: Wrath of the Gods</a></h5>
                        <span><i class="fa fa-eye"></i> 19.141 Viewes</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

