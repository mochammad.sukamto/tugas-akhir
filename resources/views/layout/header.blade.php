 <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="/index">
                            <img src="img/logo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li class="active"><a href="/index">Homepage</a></li>
                                <li><a href="/list1">Anime <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="/create1">Create</a></li>
                                        <li><a href="/list1">List</a></li>
                                    </ul>
                                </li>
                                <li><a href="/list3">User <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="/create3">Create</a></li>
                                        <li><a href="/list3">List</a></li>
                                    </ul>
                                </li>
                                <li><a href="/list2">Genre <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="/create2">Create</a></li>
                                        <li><a href="/list2">List</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="header__right">
                        <a href="#" class="search-switch"><span class="icon_search"></span></a>
                        <a href="/anime"><span class="icon_profile"></span></a>
                    </div>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>

    </header>