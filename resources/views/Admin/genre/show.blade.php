@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3" >
    <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Show Cast</h3>
            </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/cast" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleNama">Nama : </label>
                    <label> {{$cast->nama}} </label>
                  </div>
                  <div class="form-group">
                    <label for="exampleNama">Umur : </label>
                    <label> {{$cast->umur}} </label>
                  </div>
                  <div class="form-group">
                    <label for="exampleNama">Bio : </label>
                    <label> {{$cast->bio}} </label>
                  </div>
                <!-- /.card-body -->
              </form>
    </div>    
@endsection