@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3" >
    <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Create New User</h3>
            </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/user" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleNama">Username</label>
                    <input type="text" name="username" class="form-control" id="exampleInputEmail1" placeholder="Username">
                  </div>
                  <div class="form-group">
                    <label for="exampleUmur">Password</label>
                    <input type="text" name="password" class="form-control" id="exampleInputEmail1" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <label for="exampleBio">Nama</label>
                    <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="Nama">
                  </div>
                  <div class="form-group">
                    <label for="exampleBio">Email</label>
                    <input type="text" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
    </div>    
@endsection