@extends('adminlte.master')

@section('content')

<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Update User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/user/{{$data->id}}" method="POST">
              @csrf 
              @method('PUT')
              <div class="card-body">
                  <div class="form-group">
                    <label for="exampleNama">Username</label>
                    <input type="text" name="username" value="{{old('username', $data->username) }} " class="form-control" id="exampleInputEmail1" placeholder="username">
                  </div>
                  <div class="form-group">
                    <label for="exampleUmur">Password</label>
                    <input type="text" name="password" value="{{old('password', $data->password) }}" class="form-control" id="exampleInputEmail1" placeholder="password">
                  </div>
                  <div class="form-group">
                    <label for="exampleBio">Nama</label>
                    <input type="text" name="nama" value="{{old('nama', $data->nama) }}" class="form-control" id="exampleInputEmail1" placeholder="nama">
                  </div>
                  <div class="form-group">
                    <label for="exampleBio">Email</label>
                    <input type="text" name="email" value="{{old('email', $data->email) }}" class="form-control" id="exampleInputEmail1" placeholder="email">
                  </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>

@endsection