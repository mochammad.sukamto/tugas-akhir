@extends('adminlte.master')

@section('content')

<div class="card card-primary">
<div class="card-header">
  <h3 class="card-title">Show User</h3>
</div>
  <div class="card-body">
    <div class="form-group">
      <label for="username">Username : </label>
      <label for="username">{{$data->username}}</label>
      </div>
    <div class="form-group">
      <label for="password">Password : </label>
      <label for="password">{{$data->password}} </label>
    </div>
    <div class="form-group">
      <label for="name">Nama : </label>
      <label for="name">{{$data->name}} </label>
    </div>      
    <div class="form-group">
      <label for="email">Email : </label>
      <label for="email">{{$data->email}} </label>
    </div>             
  </div>
</div>

@endsection