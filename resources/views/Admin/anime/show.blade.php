@extends('adminlte.master')

@section('content')

<div class="card card-primary">
<div class="card-header">
  <h3 class="card-title">Show Anime</h3>
</div>
  <div class="card-body">
    <div class="form-group">
      <label for="judul">Judul : </label>
      <label for="judul">{{$data->judul}}</label>
      </div>
    <div class="form-group">
      <label for="deskripsi">Deskripsi : </label>
      <label for="deskripsi">{{$data->rincian}} </label>
    </div>
    <div class="form-group">
      <label for="tahun">Tahun : </label>
      <label for="tahun">{{$data->tahun}} </label>
    </div>      
    <div class="form-group">
      <label for="gambar">Gambar : </label>
      <label for="gambar">{{$data->gambar}} </label>
    </div>             
  </div>
</div>

@endsection