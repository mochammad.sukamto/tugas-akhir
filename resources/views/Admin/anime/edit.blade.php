@extends('adminlte.master')

@section('content')

<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Update Anime</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/anime/{{$data->id}}" method="POST">
              @csrf 
              @method('PUT')
              <div class="card-body">
                  <div class="form-group">
                    <label for="exampleNama">Judul</label>
                    <input type="text" name="judul" value="{{old('judul', $data->judul) }} " class="form-control" id="exampleInputEmail1" placeholder="Judul">
                  </div>
                  <div class="form-group">
                    <label for="exampleUmur">Deskripsi</label>
                    <input type="text" name="rincian" value="{{old('rincian', $data->rincian) }}" class="form-control" id="exampleInputEmail1" placeholder="Deskripsi">
                  </div>
                  <div class="form-group">
                    <label for="exampleBio">Tahun</label>
                    <input type="text" name="tahun"  class="form-control" id="exampleInputEmail1" placeholder="Tahun">
                  </div>
                  <div class="form-group">
                  <div class="form-group">
                    <label>Gambar</label>
                    <input type="file" class="form-control"  value="{{old('tahun', $data->tahun) }}" name="gambar" placeholder="gambar">
                  </div>    
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>

@endsection