@extends('adminlte.master')

@section('content')

<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Anime</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/anime" method="POST" enctype="multipart/form-data">
              @csrf 
                <div class="card-body">
                  <div class="form-group">
                    <label>Judul</label>
                    <input type="text" class="form-control"  name="judul" placeholder="Enter judul">
                  </div>
                  <div class="form-group">
                    <label>Deksripsi</label>
                    <textarea class="form-control"  name="rincian" rows="3"></textarea>
                  </div>
                  <div class="form-group">
                    <label>Tahun</label>
                    <input type="number" class="form-control"  name="tahun" placeholder="tahun">
                  </div>  
                  <div class="form-group">
                    <label>Gambar</label>
                    <input type="file" class="form-control"  name="gambar" placeholder="gambar">
                  </div>                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>

@endsection