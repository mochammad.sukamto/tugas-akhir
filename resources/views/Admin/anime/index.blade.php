@extends('adminlte.master')
@section('content')
<div class="">
      <div class="card-header">
        <h3 class="card-title">Anime</h3>
      </div>
      <!-- /.card-header -->
      <div class="card card-body">
        <div class="col-xl-2 col-xl-2 text-left">
          <div class="form-group m-form__group">
              <a href="/anime/create">
                  <button class="btn btn-success"><i class="la la-plus"></i> Tambah Anime</button>
              </a>
          </div>
      </div>
        <table class="table table-bordered table-striped">
          <thead>
              <tr>
                  <th>#</th>
                  <th>Judul</th>
                  <th>Deskripsi</th>
                  <th>Tahun</th>
                  <th>Gambar</th>
                  <th>Action</th>
              </tr>
          </thead>
          <tbody>
              
              @php $no = 1; @endphp
              @foreach($data as $datas)
                  <tr>
                      <td>{{ $no++}}</td>
                      <td>{{ $datas->judul }}</td>
                      <td>{{ $datas->rincian }}</td>
                      <td>{{ $datas->tahun }}</td>
                      <td><img src="{{ asset ('storage/images/'.$datas->gambar) }}"></td>
                      <td><a class="btn btn-success" href="/anime/{{ $datas->id }}" style="margin-right: 10px">Show</a><a class="btn btn-primary" href="/anime/{{ $datas->id }}/edit" style="margin-right: 10px">Edit</a>
                        <form method="post" style="margin-top: 10px" action="/anime/{{ $datas->id }}">
                          @csrf 
                          @method('DELETE')
                          <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                      </td>
                    </tr>
              @endforeach
          </tbody>
          </thead>
      </table>
      </div>
      <!-- /.card-body -->
    </div>
    @endsection

    @push('scripts')
    <script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}" type="text/javascript"></script>
    <script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}" type="text/javascript"></script>
<script>
  $(function () {
    console.log('tes');
    $("#example1").DataTable();
  });
</script>
@endpush