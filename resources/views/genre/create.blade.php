@extends('layout.master')

@section('content')
<div style="background-color: white">
     <div class="card-header">
                    <h3 class="card-title">Create New Genre</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form" action="/animes" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Judul Anime">
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-danger">Create</button>
                        </div>
                    </div>
                </form>


</div>
@endsection