<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Anime;
use DB;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class AnimeController extends Controller
{
    public function index(){
        $data = Anime::all();
        return view('Admin.anime.index', compact('data'));
    }

    public function create(){
        return view('Admin.anime.create');
    }
    public function Store(Request $request) {
      
        $gambar = $request->file('gambar');
        $path = Storage::putFileAs('public/images', $gambar, $gambar->getClientOriginalName());

        $file= new Anime();
        $file->judul = $request->judul;
        $file->rincian = $request->rincian;
        $file->tahun = $request->tahun;
        $file->gambar = $gambar->getClientOriginalName();
        $file->save();
    //upload image
   
    
        return redirect('/anime');
    }

    public function edit($id){
        $data = Anime::findOrFail($id);
        return view('Admin.anime.edit',compact('data'));
    }

    public function show($id){
        $data = Anime::findOrFail($id);
        return view('Admin.anime.show',compact('data'));
    }

    public function update($id,Request $request) {
        $file= Anime::findOrFail($id);
        $file->judul = $request->judul;
        $file->rincian = $request->rincian;
        $file->tahun = $request->tahun;
      
        $file->save();
      

        return redirect('/anime');
    }

    public function destroy($id){
        $data = Anime::findOrFail($id);
        $data->delete();
        return redirect('/anime');
    }   
}
