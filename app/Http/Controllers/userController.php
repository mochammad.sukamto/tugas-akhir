<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\user;
use DB;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class userController extends Controller
{
    public function index(){
        $data = user::all();
        return view('Admin.user.index', compact('data'));
    }

    public function create(){
        return view('Admin.user.create');
    }
    public function Store(Request $request) {


        $file= new user();
        $file->username = $request->username;
        $file->password = $request->password;
        $file->name = $request->name;
        $file->email = $request->email;
        $file->save();
    //upload image
   
    
        return redirect('/user');
    }

    public function edit($id){
        $data = user::findOrFail($id);
        return view('Admin.user.edit',compact('data'));
    }

    public function show($id){
        $data = user::findOrFail($id);
        return view('Admin.user.show',compact('data'));
    }

    public function update($id,Request $request) {
        $file= user::findOrFail($id);
        $file->username = $request->username;
        $file->password = $request->password;
        $file->name = $request->name;
        $file->email = $request->email;
      
        $file->save();
      

        return redirect('/user');
    }

    public function destroy($id){
        $data = user::findOrFail($id);
        $data->delete();
        return redirect('/user');
    }   
}
