<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Genre;
use DB;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class genreController extends Controller
{
    public function index(){
        $data = genre::all();
        return view('Admin.genre.index', compact('data'));
    }

    public function create(){
        return view('Admin.genre.create');
    }
    public function Store(Request $request) {      

        $file= new genre();
        $file->nama = $request->nama;
        $file->save();
    }
    public function edit($id){
        $data = genre::findOrFail($id);
        return view('Admin.genre.edit',compact('data'));
    }

    public function show($id){
        $data = genre::findOrFail($id);
        return view('Admin.genre.show',compact('data'));
    }

    public function update($id,Request $request) {
        $file= genre::findOrFail($id);
        $file-> nama = $request->nama;   
        $file->save();
      

        return redirect('/genre');
    }

    public function destroy($id){
        $data = genre::findOrFail($id);
        $data->delete();
        return redirect('/genre');
    }   
}
