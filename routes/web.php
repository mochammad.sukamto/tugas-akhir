<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
return view('/welcome');
});

Route::get('/index', function () {
    return view('/layout/index');
});

Route::get('/master', function () {
    return view('/layout/master');
});

Route::get('/create1', function () {
    return view('/anime/create');
});

Route::GET('/list1', function () {
return view('/anime/list');
});

Route::get('/create2', function () {
return view('/genre/create');
});

Route::GET('/list2', function () {
return view('/genre/list');
});

Route::get('/create3', function () {
return view('/user/create');
});

Route::GET('/list3', function () {
return view('/user/list');
});

Route::GET('/logout', function () {
return view('/layouts/app');
});

Route::get('/', 'HomeController@index');
Auth::routes();

Route::group(['middleware'=>['auth']],function(){
Route::get('/anime', 'AnimeController@index');
Route::get('/anime/create', 'AnimeController@create');
Route::post('/anime', 'AnimeController@store');
Route::get('/anime/{anime_id}', 'AnimeController@show');
Route::get('/anime/{anime_id}/edit', 'AnimeController@edit');
Route::put('/anime/{anime_id}', 'AnimeController@update');
Route::delete('/anime/{anime_id}', 'AnimeController@destroy');

Route::get('/genre', 'genreController@index');
Route::get('/genre/create', 'genreController@create');
Route::post('/genre', 'genreController@store');
Route::get('/genre/{genre_id}', 'genreController@show');
Route::get('/genre/{genre_id}/edit', 'genreController@edit');
Route::put('/genre/{genre_id}', 'genreController@update');
Route::delete('/genre/{genre_id}', 'genreController@destroy');

Route::get('/user', 'userController@index');
Route::get('/user/create', 'userController@create');
Route::post('/user', 'userController@store');
Route::get('/user/{user_id}', 'userController@show');
Route::get('/user/{user_id}/edit', 'userController@edit');
Route::put('/user/{user_id}', 'userController@update');
Route::delete('/user/{user_id}', 'userController@destroy');

    
});
